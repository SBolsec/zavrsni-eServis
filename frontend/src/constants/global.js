export const PREFIX = "eservis-";

export const ROLE_ADMIN = 1;
export const ROLE_USER = 2;
export const ROLE_SERVICE = 3;
export const ROLE_ERROR = "error";

export const AUTH_NONE = "AUTH_NONE";
export const AUTH_REQUIRED = "AUTH_REQUIRED";
export const AUTH_ANY = "AUTH_ANY";