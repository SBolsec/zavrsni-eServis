const serviceInitialState = {
  sidebarShow: 'responsive',
  loading: false,
  error: false,
  data: {}
}

export default serviceInitialState;