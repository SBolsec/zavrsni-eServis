/* Glavne kategorije */
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Svi oglasi', NULL);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Servis računala', 1);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Servis mobitela i tableta', 1);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Servis konzola', 1);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Bijela tehnika', 1);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Ostali uređaji', 1);

/* Računala - glavne kategorije */
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Popravak računala', 2);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Održavanje računala', 2);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Instalacije programa', 2);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Instalacija operativnog sistema', 2);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Čišćenje virusa', 2);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Uklanjanje prašine i zamjena termalne paste', 2);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Zamjena baterije za laptop', 2);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Popravak konektora', 2);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Ostalo', 2);

/* Mobiteli */
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Zamjena stakla', 3);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Zamjena ekrana', 3);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Zamjena baterije', 3);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Zamjena kamere', 3);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Zamjena poklopca', 3);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Zamjena konektora punjenja', 3);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Zamjena zvučnika i senzora', 3);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Zamjena matične ploče', 3);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Popravak softwera', 3);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Otključavanje', 3);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Ostalo', 3);

/* Konzole  */
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Playstation 1', 4);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Playstation 2', 4);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Playstation 3', 4);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Playstation 4', 4);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Playstation 5', 4);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Playstation Portable', 4);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Playstation Vita', 4);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Xbox 360', 4);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Xbox One', 4);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Nintendo Wii', 4);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Nintendo Wii U', 4);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Nintendo 3DS', 4);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Nintendo Switch', 4);

/* Bijela tehnika */
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Perilice posuđa', 5);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Perilice rublja', 5);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Sučilice rublja', 5);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Hladnjaci', 5);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Pećnice', 5);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Štednjaci', 5);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Nape', 5);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Zamrzivači', 5);
INSERT INTO kategorija_kvara (naziv_kategorija_kvara, sif_roditelj) VALUES ('Bojleri', 5);